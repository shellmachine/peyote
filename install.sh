#!/bin/sh
# vim: ts=2 sw=2 sts=2 et
#
# simple peyote setup script
# written by Arminius <armin@arminius.org>
#
# This file is part of peyote for zsh.
#

# parameter expansion to extract the scripts home directory
home="${0%/*}"

# cd to parent location folder
cd "$home"

# print a message
msg () {
	printf "[peyote] "
  printf "$@ \n"
}

if ! hash go >/dev/null 2>&1; then
  msg "go seems to not be installed, exiting gracefully."
  msg "Please install the go package for your distribution."
  exit 255
fi

bailout () {
  printf "ERROR: $@\n"
  exit 255
}

# file/directory exists error message
fileexists () {
  if [ -e "$1" ]; then
    msg "File or directory exists: $1"
    msg "I'm not going to overwrite it, so you have to resolve this manually."
    msg "Exiting gracefully. Goodbye."
    exit 1
  fi
}

# check for conflicts
fileexists "$HOME/.peyote"
fileexists "$HOME/.zshrc"

mkdir -p "$HOME/.peyote"
cp -R ./ "$HOME/.peyote"
cd

ln -s .peyote/zshrc .zshrc || bailout 'Symbolic link ~/.zshrc exists, I will not touch this for you.'
echo
msg "Setup complete."
msg "peyote has been installed to ~/.peyote"
msg "Keep in mind you have to build the peyote binary (go build peyote.go)"
echo
msg "Optionally, you can install the zsh-syntax-highlighting ZSH plugin by"
msg "cloning https://github.com:zsh-users/zsh-syntax-highlighting to ~/git/zsh-syntax-highlighting"
echo
msg "Feel free to edit these path names from your ~/zshrc file."
echo




