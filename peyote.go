package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

const (
	prompttextcolor                = "254"
	promptuserbackgroundcolor      = "24"
	promptrootbackgroundcolor      = "88"
	promptdirectorybackgroundcolor = "29"
	promptgittextcolor             = "2"
	promptgitbackgroundcolor       = "234"
	prompterrortextcolor           = "203"
	prompterrorbackgroundcolor     = "236"

	showuserandhost    = true
	showdirectoryname  = true
	shortdirectoryname = false
	showgitstatus      = true
	showreturncode     = true
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Usage: peyote $? $EUID")
		os.Exit(1)
	}

	returncode := os.Args[1]
	username := os.Getenv("USER")
	euid := os.Args[2]
	euidnum, err := strconv.Atoi(euid)
	if err != nil {
		log.Fatal(err)
	}

	var usercolor string
	if euidnum > 0 {
		usercolor = promptuserbackgroundcolor
	} else {
		usercolor = promptrootbackgroundcolor
	}

	hostname, err := os.Hostname()
	dir, err := os.Getwd()

	if err != nil {
		log.Fatal(err)
	}

	homedir := os.Getenv("HOME")
	dirname := strings.Replace(dir, homedir, "~", 1)

	if shortdirectoryname && strings.Count(dirname, "/") > 1 {
		dirname = dirname[strings.LastIndex(dirname, "/")+1:]
	}

	returncodeint, err := strconv.Atoi(returncode)
	if err != nil {
		log.Fatal(err)
	}

	prompt := newprompt()
	if showuserandhost {
		prompt.addsegment(newsegment(prompttextcolor, usercolor, fmt.Sprintf(" %s@%s ", username, hostname)))
	}
	if showdirectoryname {
		prompt.addsegment(newsegment(prompttextcolor, promptdirectorybackgroundcolor, fmt.Sprintf(" %s ", dirname)))
	}
	if showgitstatus {
		directoryisgitrepo := true
		gitstatus := exec.Command("git", "status")
		output, err := gitstatus.CombinedOutput()
		if err != nil {
			directoryisgitrepo = false
		}
		if directoryisgitrepo {
			githead := ""
			workingtreeclean := "!"
			scanner := bufio.NewScanner(bytes.NewReader(output))
			for scanner.Scan() {
				text := scanner.Text()
				if strings.HasPrefix(text, "On branch ") {
					githead = strings.Split(text, "On branch ")[1]
				}
				if strings.HasPrefix(text, "HEAD detached at ") {
					githead = strings.Split(text, "HEAD detached at ")[1]
				}
				if strings.HasPrefix(text, "HEAD detached from ") {
					githead = strings.Split(text, "HEAD detached from ")[1]
				}
				if strings.HasPrefix(text, "nothing to commit, working tree clean") {
					workingtreeclean = ""
				}
			}
			if err := scanner.Err(); err != nil {
				log.Fatal(err)
			}
			prompt.addsegment(newsegment(promptgittextcolor, promptgitbackgroundcolor, fmt.Sprintf(" %[1]s%[2]s ", githead, workingtreeclean)))
		}
	}
	if showreturncode && returncodeint > 0 {
		prompt.addsegment(newsegment(prompterrortextcolor, prompterrorbackgroundcolor, ""))
		prompt.addsegment(newsegment(prompterrortextcolor, prompterrorbackgroundcolor, fmt.Sprintf(" %s ", returncode)))
	}

	fmt.Println(prompt.ToString())
}

type prompt struct {
	segments []segment
}

func newprompt(segmentsparams ...[]string) prompt {
	segments := []segment{}
	for _, segmentParams := range segmentsparams {
		segments = append(segments, newsegment(segmentParams[0], segmentParams[1], segmentParams[2]))
	}
	return prompt{
		segments: segments,
	}
}

func (p *prompt) addsegment(segment segment) *prompt {
	p.segments = append(p.segments, segment)
	return p
}

func (p *prompt) ToString() string {
	fmtString := ""
	for i := range p.segments {
		nextbackgroundcolor := ""
		if i < len(p.segments)-1 {
			nextbackgroundcolor = p.segments[i+1].backgroundcolor
		}
		fmtString += p.segments[i].ToString(nextbackgroundcolor)
	}
	fmtString += "%f%k "
	return fmtString
}

type segment struct {
	TextColor       string
	backgroundcolor string
	Text            string
}

func newsegment(textcolor, backgroundcolor, text string) segment {
	return segment{
		TextColor:       textcolor,
		backgroundcolor: backgroundcolor,
		Text:            text,
	}
}

func (s *segment) ToString(nextbackgroundcolor string) string {
	var powerlinearrow string
	if nextbackgroundcolor == "" {
		// powerlinearrow = fmt.Sprintf("%%F{%[1]s}", s.backgroundcolor)
		powerlinearrow = fmt.Sprintf("%%F{%[1]s}", s.backgroundcolor)
	} else if nextbackgroundcolor == s.backgroundcolor {
		// powerlinearrow = fmt.Sprintf("%%K{%[1]s}%%F{%[2]s}", nextbackgroundcolor, s.TextColor)
		powerlinearrow = fmt.Sprintf("%%K{%[1]s}%%F{%[2]s}", nextbackgroundcolor, s.TextColor)
	} else {
		// powerlinearrow = fmt.Sprintf("%%K{%[1]s}%%F{%[2]s}", nextbackgroundcolor, s.backgroundcolor)
		powerlinearrow = fmt.Sprintf("%%K{%[1]s}%%F{%[2]s}", nextbackgroundcolor, s.backgroundcolor)
	}
	return fmt.Sprintf("%%F{%[1]s}%%K{%[2]s}%[3]s%%f%%k%[4]s", s.TextColor, s.backgroundcolor, s.Text, powerlinearrow)
}






