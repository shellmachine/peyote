# peyote
peyote is a mescaline remake, written in the Go programming language for better performance

Right now this is just a proof-of-concept prototype implementation / WIP. See screenshot for details.

![Screenshot](/img/shot.png)

## Known bugs
There is a known bug with some terminal emulators when using ZLE_RPROMPT_INDENT=1 - urxvt is known to interprete this correctly, though.


